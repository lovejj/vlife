package cn.wwwlike.auth.service;

import cn.wwwlike.auth.dao.SysFilterGroupDao;
import cn.wwwlike.auth.entity.SysFilterGroup;
import cn.wwwlike.vlife.core.VLifeService;
import org.springframework.stereotype.Service;

@Service
public class SysFilterGroupService extends VLifeService<SysFilterGroup, SysFilterGroupDao> {

}
