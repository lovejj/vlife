package cn.wwwlike.auth.dao;

import cn.wwwlike.auth.entity.SysFilter;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class SysFilterDao extends DslDao<SysFilter> {
}
