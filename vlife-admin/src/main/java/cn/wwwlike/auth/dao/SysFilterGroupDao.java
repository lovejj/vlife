package cn.wwwlike.auth.dao;

import cn.wwwlike.auth.entity.SysFilterGroup;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class SysFilterGroupDao extends DslDao<SysFilterGroup> {
}
