package cn.wwwlike.auth.dao;

import cn.wwwlike.auth.entity.SysFilterDetail;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class SysFilterDetailDao extends DslDao<SysFilterDetail> {
}
