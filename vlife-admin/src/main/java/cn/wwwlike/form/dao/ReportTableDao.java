package cn.wwwlike.form.dao;

import cn.wwwlike.form.entity.ReportTable;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class ReportTableDao extends DslDao<ReportTable> {
}
