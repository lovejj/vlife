package cn.wwwlike.form.dao;

import cn.wwwlike.form.entity.PageLayout;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class PageLayoutDao extends DslDao<PageLayout> {
}
