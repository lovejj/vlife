package cn.wwwlike.form.dao;

import cn.wwwlike.form.entity.FormCondition;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class FormConditionDao extends DslDao<FormCondition> {
}
