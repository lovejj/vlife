package cn.wwwlike.form.dao;

import cn.wwwlike.form.entity.ReportItem;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class ReportItemDao extends DslDao<ReportItem> {
}
