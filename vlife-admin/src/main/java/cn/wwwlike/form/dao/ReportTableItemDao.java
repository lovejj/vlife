package cn.wwwlike.form.dao;

import cn.wwwlike.form.entity.ReportTableItem;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class ReportTableItemDao extends DslDao<ReportTableItem> {
}
