package cn.wwwlike.form.dao;

import cn.wwwlike.form.entity.ReportKpi;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class ReportKpiDao extends DslDao<ReportKpi> {
}
