package cn.wwwlike.form.dao;

import cn.wwwlike.form.entity.FormGroup;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class FormGroupDao extends DslDao<FormGroup> {
}
