package cn.wwwlike.form.service;

import cn.wwwlike.form.dao.ReportKpiDao;
import cn.wwwlike.form.entity.ReportKpi;
import cn.wwwlike.vlife.core.VLifeService;
import org.springframework.stereotype.Service;

@Service
public class ReportKpiService extends VLifeService<ReportKpi, ReportKpiDao> {
}
