package cn.wwwlike.form.service;

import cn.wwwlike.form.dao.ReportTableItemDao;
import cn.wwwlike.form.entity.ReportTableItem;
import cn.wwwlike.vlife.core.VLifeService;
import org.springframework.stereotype.Service;

@Service
public class ReportTableItemService extends VLifeService<ReportTableItem, ReportTableItemDao> {
}
