package cn.wwwlike.form.service;

import cn.wwwlike.form.dao.FormReactionDao;
import cn.wwwlike.form.entity.FormReaction;
import cn.wwwlike.vlife.core.VLifeService;
import org.springframework.stereotype.Service;

@Service
public class FormReactionService extends VLifeService<FormReaction, FormReactionDao> {
}
