package cn.wwwlike.form.service;

import cn.wwwlike.form.dao.PageLayoutDao;
import cn.wwwlike.form.entity.PageLayout;
import cn.wwwlike.vlife.core.VLifeService;
import org.springframework.stereotype.Service;

@Service
public class PageLayoutService extends VLifeService<PageLayout, PageLayoutDao> {
}
