package cn.wwwlike.form.service;

import cn.wwwlike.form.dao.ReportItemDao;
import cn.wwwlike.form.entity.ReportItem;
import cn.wwwlike.vlife.core.VLifeService;
import org.springframework.stereotype.Service;

@Service
public class ReportItemService extends VLifeService<ReportItem, ReportItemDao> {
}
