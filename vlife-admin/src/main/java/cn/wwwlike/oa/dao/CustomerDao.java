package cn.wwwlike.oa.dao;

import cn.wwwlike.oa.entity.Customer;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerDao extends DslDao<Customer> {
}
